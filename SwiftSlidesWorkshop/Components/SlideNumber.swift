//
//  SlideNumber.swift
//  SwiftSlides
//
//  Created by Matthew Young on 3/29/23.
//

import SwiftUI

struct SlideNumber: View {
    
    var slideNumber: Int
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Text(String(slideNumber))
                    .font(.largeTitle)
            }
            Spacer()
        }
        .padding(.trailing)
    }
}

struct SlideNumber_Previews: PreviewProvider {
    static var previews: some View {
        SlideNumber(slideNumber: 0)
    }
}

//
//  SlideText.swift
//  SwiftSlides
//
//  Created by Matthew Young on 3/30/23.
//

import SwiftUI

struct SlideText: View {
    
    var text: String
    
    var body: some View {
        HStack {
            Text(text)
                .font(Font.custom("Gibson-Semibold", size: 48))
                .multilineTextAlignment(.leading)
            Spacer()
        }
    }
}

struct SlideText_Previews: PreviewProvider {
    static var previews: some View {
        SlideText(text: "Slide text")
    }
}

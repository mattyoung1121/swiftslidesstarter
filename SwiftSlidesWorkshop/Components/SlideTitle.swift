//
//  SlideTitle.swift
//  SwiftSlides
//
//  Created by Matthew Young on 3/30/23.
//

import SwiftUI

struct SlideTitle: View {
    
    var text: String
    
    var body: some View {
        HStack {
            Text(text)
                .font(Font.custom("Gibson-Semibold", size: 80))
            
            Spacer()
        }
    }
}

struct SlideTitle_Previews: PreviewProvider {
    static var previews: some View {
        SlideTitle(text: "Slide title")
    }
}

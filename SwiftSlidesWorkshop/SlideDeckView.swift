//
//  SlideDeckView.swift
//  SwiftSlides
//
//  Created by Matthew Young on 5/8/23.
//

import Foundation
import SwiftUI

struct SlideDeckView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct SlideDeckView_Previews: PreviewProvider {
    static var previews: some View {
        SlideDeckView()
    }
}

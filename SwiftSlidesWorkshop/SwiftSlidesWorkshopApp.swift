//
//  SwiftSlidesWorkshopApp.swift
//  SwiftSlidesWorkshop
//
//  Created by Matthew Young on 5/9/23.
//

import SwiftUI

@main
struct SwiftSlidesWorkshopApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
